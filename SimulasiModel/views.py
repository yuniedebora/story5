from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Schedule_Form
from .models import Schedule
from datetime import datetime

# Create your views here.
def index(request):
    listJadwal = {
        'listJadwal' : Schedule.objects.all()
    }
    return render(request, 'SimulasiModel/Model.html', listJadwal)


def create(request):
    createJadwal = Schedule_Form(request.POST or None)
    if (request.method == 'POST' and createJadwal.is_valid()) :
        simpen = Schedule(
            activity = createJadwal.data['activity'],
            date = datetime.strptime('{} {} {} {}'.format(createJadwal.data['date_month'], createJadwal.data['date_day'], createJadwal.data['date_year'], createJadwal.data['time']), '%m %d %Y %H:%M'),
            time = createJadwal.data['time'],
            place = createJadwal.data['place'],
            category = createJadwal.data['category'],
        )
        simpen.save()
        return HttpResponseRedirect("/Schedule/")
    create_Jadwal ={
        'schedule_form' : createJadwal,
    }
    return render(request, 'SimulasiModel/create.html', create_Jadwal)

def delete(request, delete_id):
    Schedule.objects.filter(id = delete_id).delete()
    return HttpResponseRedirect("/Schedule/")

