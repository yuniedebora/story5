from django.urls import path, re_path
from . import views

app_name = 'activity'
urlpatterns = [
    path('', views.index, name = 'schedule'),
    path('create/', views.create, name = 'create'),
    re_path('delete/(?P<delete_id>.*)/', views.delete, name='delete'),
]