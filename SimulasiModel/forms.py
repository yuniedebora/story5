from django import forms
from datetime import datetime

class Schedule_Form(forms.Form):
    activity = forms.CharField(max_length=100)
    date = forms.DateField( widget = forms.SelectDateWidget(attrs={'id' : 'datepicker'}))
    time = forms.TimeField( widget = forms.TimeInput(attrs = {'id' : 'timepicker'}))
    place = forms.CharField(max_length=100)
    category = forms.CharField(max_length=100)
