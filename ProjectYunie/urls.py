
from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name = 'index'),
    path('Profile/', include('Profile.urls')),
    path('Gallery/', include('Gallery.urls')),
    path('Schedule/', include ('SimulasiModel.urls')),
]
